#Written by: Ricardo R. Williams II

#Brief Synopsis

#This application utilizes flask and docker to create and search a server for certain files
#If the file queried exists in the server, then it will return the html page requested with a 200 web response
#If it does not find such a file, it will return with a 404 error as well as an appropriate html file
#Flask is what is being used to create the web server and app.py is the command and control center
#app.py in web folder does our error handliing and deals with the responses
#Docker is just a means of implementing a standard to ensure that this code can run on many different machines

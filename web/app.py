from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/<name>')

def hello(name):
    try:
        return render_template(name), 200

    except:
        if("~" in name or ".." in name):
            return render_template('403.html'), 403
        else:
            return render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
